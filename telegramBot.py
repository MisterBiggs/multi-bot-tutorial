# Works with Python 3.7
import logging
import os

import telegram
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

from functions import about, nextLaunch, roadster

TELEGRAM_TOKEN = "TOKEN"

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)
print("Bot Online")


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    """Send a message when the command /start is issued."""
    update.message.reply_text("I am started and ready to go!")


def nextRocketLaunch(bot, update):
    launch = nextLaunch()

    message = f"The **{launch['mission']}** is in {launch['countdown']}\n\n"
    message += f"The Mission is using the {launch['rocket']} and you can watch the mission live [here]({launch['video']})"

    update.message.reply_text(text=message, parse_mode=telegram.ParseMode.MARKDOWN)


def roadster_reply(bot, update):
    roadster_data = roadster()
    message = f"[{roadster_data['name']}]({roadster_data['wikipedia']})\n"
    message += f"Rocket launch was {roadster_data['days_passed']} ago.\n\n{roadster_data['details']}"
    update.message.reply_text(text=message, parse_mode=telegram.ParseMode.MARKDOWN)


def about_reply(bot, update):
    about_data = about()
    message = f"**About**\n"
    message += f"{about_data['description']}\n\n"
    message += f"[Blog Post]({about_data['blogPost']})\n"
    message += f"[Repo]({about_data['repo']})"
    update.message.reply_text(text=message, parse_mode=telegram.ParseMode.MARKDOWN)


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(TELEGRAM_TOKEN)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("launch", nextRocketLaunch))
    dp.add_handler(CommandHandler("roadster", roadster_reply))
    dp.add_handler(CommandHandler("about", about_reply))

    # log all errorsc
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
