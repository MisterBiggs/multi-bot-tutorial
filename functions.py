import requests
from datetime import datetime, timedelta


def nextLaunch():
    """Uses the launchlibrary.net api to get the next rocket launch and returns
    a dictionary with all the important information.

    Example Output:
    {
    "rocket": "Proton-M/Blok DM-03 ",
    "mission": " Spektr-RG",
    "date": "July 12, 2019 12:31:00 UTC",
    "countdown": "14 hours and 48 minutes.",
    "video": "https://www.youtube.com/watch?v=Dy1nkGghEVk",
    }
    """

    url = "https://launchlibrary.net/1.4/launch/next/1?mode=list"
    response = requests.get(url)

    if response.status_code is 200:
        data = response.json()["launches"][0]

        rocket, mission = data["name"].split("|")
        date = data["net"]

        # Extract the timde delta between now and the launch, then format it into an easy to read format.
        countdown = str(
            datetime.strptime(date, "%B %d, %Y %H:%M:%S %Z") - datetime.today()
        ).split(":")
        countdown = f"{countdown[0]} hours and {countdown[1]} minutes."

    else:
        print("error")

    launch = {
        "rocket": rocket.strip(),
        "mission": mission.strip(),
        "date": date,
        "countdown": countdown,
        # "video": data["vidURLs"][0],
    }
    return launch


def roadster():
    """
    Uses SpaceX Roadster api to get info on the Tesla Roadster they launched. Returns api almost verbatim as a dictionary.

    example output:
    {
    "name": "Elon Musk's Tesla Roadster",
    "launch_date_utc": "2018-02-06T20:45:00.000Z",
    "launch_date_unix": 1517949900,
    "launch_mass_kg": 1350,
    "launch_mass_lbs": 2976,
    "norad_id": 43205,
    "epoch_jd": 2458683.801539352,
    "orbit_type": "heliocentric",
    "apoapsis_au": 1.663739918024834,
    "periapsis_au": 0.9860559106037544,
    "semi_major_axis_au": 342.7339983025085,
    "eccentricity": 0.2557495185475542,
    "inclination": 1.077527011891435,
    "longitude": 317.0906896840899,
    "periapsis_arg": 177.4801624528808,
    "period_days": 557.0216193260303,
    "speed_kph": 118414.54800000001,
    "speed_mph": 73579.36610530801,
    "earth_distance_km": 302138044.286228,
    "earth_distance_mi": 187739818.7161778,
    "mars_distance_km": 150748511.13255098,
    "mars_distance_mi": 93670753.11094435,
    "flickr_images": [
        "https://farm5.staticflickr.com/4615/40143096241_11128929df_b.jpg",
        "https://farm5.staticflickr.com/4702/40110298232_91b32d0cc0_b.jpg",
        "https://farm5.staticflickr.com/4676/40110297852_5e794b3258_b.jpg",
        "https://farm5.staticflickr.com/4745/40110304192_6e3e9a7a1b_b.jpg",
    ],
    "wikipedia": "https://en.wikipedia.org/wiki/Elon_Musk%27s_Tesla_Roadster",
    "details": "Elon Musk's Tesla Roadster is an electric sports car that served as the dummy payload for the February 2018 Falcon Heavy test flight and is now an artificial satellite of the Sun. Starman, a mannequin dressed in a spacesuit, occupies the driver's seat. The car and rocket are products of Tesla and SpaceX. This 2008-model Roadster was previously used by Musk for commuting, and is the only consumer car sent into space.",
    "days_passed": "527 days",
    }
    """
    response = requests.get("https://api.spacexdata.com/v3/roadster")

    if response.status_code is 200:
        data = response.json()
        data["days_passed"] = str(
            timedelta(seconds=datetime.now().timestamp() - data["launch_date_unix"])
        ).split(",")[0]

    else:
        print("error")

    return data


def about():
    """
    Just returns information about the bot.
    """
    return {
        "description": "This is a simple bot used to demonstrate how to easily port the same bot to multiple platforms such as Discord and Telegram",
        "functions": [
            (
                "roadster",
                "Provides simple information about the Tesla Roadster launched into space by Elon Musk as a dummy payload to test the Falcon Heavy Rocket",
            ),
            ("launch", "Provides details on the next rocket launch happening."),
            ("about", "Gives information about the bot."),
        ],
        "repo": "https://gitlab.com/MisterBiggs/multi-bot-tutorial",
        "blogPost": "https://blog.ansonbiggs.com/",
    }

