# Writing Discord and Telegram bots at the same time using Python

Writing bots is a great way to learn Python. It can combine many areas of learning and can usually produce something thats useful to you and maybe even some friends. The aim of this guide is to show how you can write nearly identical Telegram and Discord bots (or maybe even a bot for a different platform) while keeping the main chunk of your code reusable between the two. The topics covered in this guide are as follows:

- [Writing Discord and Telegram bots at the same time using Python](#Writing-Discord-and-Telegram-bots-at-the-same-time-using-Python)
- [Writing code that works for Discord and Telegram](#Writing-code-that-works-for-Discord-and-Telegram)
- [How to write a Telegram Bot](#How-to-write-a-Telegram-Bot)
- [How to Write a Discord Bot](#How-to-Write-a-Discord-Bot)
- [How to run bots in Docker containers](#How-to-run-bots-in-Docker-containers)
- [How to keep your api keys secure, while still sharing your code](#How-to-keep-your-api-keys-secure-while-still-sharing-your-code)

## Writing code that works for Discord and Telegram

## How to write a Telegram Bot

## How to Write a Discord Bot

## How to run bots in Docker containers

## How to keep your api keys secure, while still sharing your code
