import discord
from functions import nextLaunch, roadster, about

client = discord.Client()


@client.event
async def on_ready():
    print("We have logged in as {0.user}".format(client))


@client.event
async def on_message(message):

    # Prevent bot from replying to itself
    if message.author == client.user:
        return

    if message.content.startswith("/launch"):
        launch = nextLaunch()
        embed = discord.Embed(
            title=f"Mission: {launch['mission']}",
            description=f"Rocket: {launch['rocket']}",
            color=0x069AD2,
        )
        embed.add_field(name="Launch Date", value=launch["date"], inline=False)
        embed.add_field(
            name="Countdown to Launch", value=launch["countdown"], inline=False
        )
        await message.channel.send(embed=embed)

    elif message.content.startswith("/roadster"):
        roadster_data = roadster()

        embed = discord.Embed(
            title=roadster_data["name"],
            url=roadster_data["wikipedia"],
            description=f"Launched {roadster_data['days_passed']} ago.",
            color=0xFF0000,
        )

        embed.add_field(
            name="Distance From Earth",
            value=f"{round(roadster_data['earth_distance_km'])} (km)",
            inline=True,
        )
        embed.add_field(
            name="Distance From Mars",
            value=f"{round(roadster_data['mars_distance_km'])} (km)",
            inline=True,
        )
        embed.set_footer(text="Thanks Elon <3")
        await message.channel.send(embed=embed)

    elif message.content.startswith("/about"):
        about_data = about()
        embed = discord.Embed(
            title="About",
            url=about_data["blogPost"],
            description=about_data["description"],
            color=0xFF0000,
        )
        for func in about_data["functions"]:
            embed.add_field(name=f"/{func[0]}", value=func[1], inline=True)

        await message.channel.send(embed=embed)


client.run("TOKEN")

